<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Owner_model extends CI_Model
{

    var $table = 'user';
    var $column_order = array('user_fname', 'user_lname', 'phone', 'email', 'user_status', 'created_at'); //set column field database for datatable orderable
    var $column_search = array('user_fname', 'user_lname', 'phone', 'email'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('user_id' => 'desc'); // default order


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->order = array('created_at' => 'desc'); // latest on top
    }

    private function _get_datatables_query()
    {

        $this->db->from($this->table);
        $this->db->where('role_type', 1);//only ownwer

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        $this->db->where('role_type', 1);//only ownwer

        $query = $this->db->get();
        return $query->num_rows();

        //return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('user_id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete($this->table);
    }

    public function getOwnerEmailList()
    {
        $email_list = array();

        $this->db->select('email');
        $this->db->from($this->table);

        $this->db->where('role_type', 1);//only ownwer
        $this->db->where('email!=','');
        $this->db->where('email!=',null);

        $query = $this->db->get();

        $result = $query->result_array();

        if (!empty($result)) {
            $email_list = array_unique(array_column($result, 'email')) ;
        }

        return $email_list;
    }


}