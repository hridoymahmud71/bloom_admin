<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_settings_model extends CI_Model
{

    var $table = 'settings';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function setSettings($settings_type, $settings_key, $settings_val)
    {
        $settings_row = $this->getSettings($settings_type, $settings_key, $only_value = false);//check if the entire row exists

        if (empty($settings_row)) {
            $ins_data = array();
            $ins_data['settings_type'] = $settings_type;
            $ins_data['settings_key'] = $settings_key;
            $ins_data['settings_val'] = $settings_val;

            $this->db->insert($this->table, $ins_data);

        }
        if (!empty($settings_row)) {
            $upd_data = array();
            $upd_data['settings_val'] = $settings_val;
            $this->db->update($this->table, $upd_data, array('settings_type' => $settings_type, 'settings_key' => $settings_key));
        }
    }

    public function getSettings($type, $key, $only_value)
    {
        $ret = null;

        $this->db->select('*');
        $this->db->from($this->table);

        $this->db->where('settings_type', $type);
        $this->db->where('settings_key', $key);

        $query = $this->db->get();

        $row = $query->row_array();

        if (!empty($row)) {
            $ret = $row['settings_val'];
        }

        return $only_value ? $ret : $row;

    }

    public function getStripeSettings()
    {
        $stripe_settings = array();
        $stripe_settings_list = $this->getSettingsList("stripe");

        if (!empty($stripe_settings_list)) {
            foreach ($stripe_settings_list as $stripe_settings_list_item) {
                if (!empty($stripe_settings_list_item['settings_key'])) {
                    $stripe_settings[$stripe_settings_list_item['settings_key']] = $stripe_settings_list_item['settings_val'];
                }
            }
        }

        return $stripe_settings;
    }

    public function getSettingsList($type)
    {
        $this->db->select('*');
        $this->db->from($this->table);

        $this->db->where('settings_type', $type);

        $query = $this->db->get();
        return $query->result_array();

    }


    public function get_stripe_test_mode()
    {
        $test_mode = 'on';
        $res = $this->getSettings('stripe', 'test_mode',$only_value = true);

        if (!empty($res)) {
            $test_mode = $res;
        }

        return $test_mode;
    }

    public function get_stripe_secret_key()
    {
        $test_mode = $this->get_stripe_test_mode();
        $key = $test_mode == 'on' ? 'tsk' : 'lsk';
        return $this->getSettings('stripe', $key,$only_value = true);

    }

    public function get_stripe_public_key()
    {
        $test_mode = $this->get_stripe_test_mode();
        $key = $test_mode == 'on' ? 'tpk' : 'lpk';
        return $this->getSettings('stripe', $key,$only_value = true);

    }


}