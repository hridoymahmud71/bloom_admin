<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property_model extends CI_Model
{

    var $table = 'property';
    var $column_order = array('user_fname','phone', 'email', 'property_address', 'city', 'zip_code','state_name', 'created_at'); //set column field database for datatable orderable
    var $column_search = array('user_fname','phone', 'email', 'property_address', 'city', 'zip_code','state_name', 'created_at'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('p.created_at' => 'desc'); // default order

    var $property_table = array('property_address', 'city', 'zip_code', 'created_at',);
    var $user_table = array('user_fname','phone', 'email');
    var $state_table = array('state_name');

    var $numeric_user_table = array(0, 1, 2);
    var $numeric_property_table = array(3, 4, 5, 7);
    var $numeric_state_table = array(6);

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $this->db->select("p.*,u.user_fname,u.user_lname,u.email,u.phone,s.state_id,s.state_name");
        $this->db->from("property as p");
        $this->db->join("user as u", "p.user_id=u.user_id", "left");
        $this->db->join("state as s", "s.state_id=p.state");

        $prefix = "";

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            $prefix = '';
            if (in_array($item, $this->property_table)) {
                $prefix = 'p.';
            } else if (in_array($item, $this->user_table)) {
                $prefix = 'u.';
            } else if (in_array($item, $this->state_table)) {
                $prefix = 's.';
            } else {
                $prefix = '';
            }

            if ($_POST['search']['value']) // if datatable send POST for search
            {


                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.

                    if ($item == 'user_fname') {
                        $this->db->like('concat(u.user_fname," ",u.user_lname)', $_POST['search']['value']);
                    } else {
                        $this->db->like($prefix . $item, $_POST['search']['value']);
                    }

                } else {
                    if ($item == 'user_fname') {
                        $this->db->or_like('concat(u.user_fname," ",u.user_lname)', $_POST['search']['value']);
                    } else {
                        $this->db->or_like($prefix . $item, $_POST['search']['value']);
                    }
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $pref = '';

            if (in_array($_POST['order']['0']['column'], $this->numeric_property_table)) {
                $pref = 'p.';
            } else if (in_array($_POST['order']['0']['column'], $this->user_table)) {
                $pref = 'u.';
            } else if (in_array($_POST['order']['0']['column'], $this->numeric_state_table)) {
                $pref = 's.';
            } else {
                $pref = '';
            }

            $this->db->order_by($pref . $this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->select("p.*,u.user_fname,u.user_lname,u.email,u.phone,s.state_id,s.state_name");
        $this->db->from("property as p");
        $this->db->join("user as u", "p.user_id=u.user_id", "left");
        $this->db->join("state as s", "s.state_id=p.state");

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('property_id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('property_id', $id);
        $this->db->delete($this->table);
    }

    //------------------------------------------------------------------------------------------------------------------

    public function getStatesByCountry($country_id)
    {
        $this->db->select("*");
        $this->db->from("state");
        $this->db->where("country_id", $country_id);

        $query = $this->db->get();
        return $query->result();
    }

}