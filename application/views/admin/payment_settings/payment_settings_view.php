<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Payment Settings(Stripe)
                        </h3>
                    </div>
                    <?php if (!empty($error_message)) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <?= $error_message ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <form action="paymentsettings/process_settings" id="settings-form" class="form-horizontal"
                              method="post">
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Monthly plan 1 name</label>
                                    <div class="col-md-6">
                                        <input id="monthly_plan_1_name" name="monthly_plan_1_name"
                                               placeholder="Enter the name of monthly plan 1"
                                               class="form-control"
                                               value="<?= $monthly_plan_1_name ?>"
                                               loaded-name="<?= $monthly_plan_1_name ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="monthly_plan_1_name_trigger" class=""
                                               value=""
                                        >
                                        <label style="padding: 2px 2px;margin:2px 2px" class="control-label ">
                                            Choose stripe nickname of monthly plan 1
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Monthly plan 1</label>
                                    <div class="col-md-6">
                                        <select id="monthly_plan_1_id" name="monthly_plan_1_id" class="form-control">
                                            <option value="">Choose a plan</option>
                                            <?php if (!empty($plans)) { ?>
                                                <?php foreach ($plans as $plan) { ?>
                                                    <option value="<?= $plan->id ?>" <?= $monthly_plan_1_id == $plan->id ? " selected " : "" ?> >
                                                        <?= $plan->nickname ?>
                                                    </option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Yearly plan 1 name</label>
                                    <div class="col-md-6">
                                        <input id="yearly_plan_1_name" name="yearly_plan_1_name"
                                               placeholder="Enter the name of yearly plan 1"
                                               class="form-control"
                                               value="<?= $yearly_plan_1_name ?>"
                                               loaded-name="<?= $yearly_plan_1_name ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="yearly_plan_1_name_trigger" class=""
                                               value=""
                                        >
                                        <label style="padding: 2px 2px;margin:2px 2px" class="control-label">
                                            Choose stripe nickname of yearly plan 1
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Yearly plan 1</label>
                                    <div class="col-md-6">
                                        <select id="yearly_plan_1_id" name="yearly_plan_1_id" class="form-control">
                                            <option value="">Choose a plan</option>
                                            <?php if (!empty($plans)) { ?>
                                                <?php foreach ($plans as $plan) { ?>
                                                    <option value="<?= $plan->id ?>" <?= $yearly_plan_1_id == $plan->id ? " selected " : "" ?> >
                                                        <?= $plan->nickname ?>
                                                    </option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Test mode</label>
                                    <div class="col-md-6">
                                        <select name="test_mode" class="form-control">
                                            <option value="">--Select mode--</option>
                                            <option value="on" <?= $test_mode == "on" ? " selected " : "" ?> >Test
                                            </option>
                                            <option value="off" <?= $test_mode == "off" ? " selected " : "" ?> >Live
                                            </option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Test secret key</label>
                                    <div class="col-md-6">
                                        <input name="tsk" placeholder="Enter test secret key" class="form-control"
                                               value="<?= $tsk ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Test public key</label>
                                    <div class="col-md-6">
                                        <input name="tpk" placeholder="Enter test public key" class="form-control"
                                               value="<?= $tpk ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Live secret key</label>
                                    <div class="col-md-6">
                                        <input name="lsk" placeholder="Enter live secret key" class="form-control"
                                               value="<?= $lsk ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Live public key</label>
                                    <div class="col-md-6">
                                        <input name="lpk" placeholder="Enter live public key" class="form-control"
                                               value="<?= $lpk ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Signing secret</label>
                                    <div class="col-md-6">
                                        <input name="signing_secret" placeholder="Enter signing secret for webhook" class="form-control"
                                               value="<?= $signing_secret ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-success btn-lg btn-block">Set</button>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </section>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        var $monthly_plan_1_name_trigger = $("#monthly_plan_1_name_trigger");
        var $yearly_plan_1_name_trigger = $("#yearly_plan_1_name_trigger");

        var $monthly_plan_1_name = $("#monthly_plan_1_name");
        var $yearly_plan_1_name = $("#yearly_plan_1_name");
        var monthly_plan_1_loaded_name = $monthly_plan_1_name.attr("loaded-name");
        var yearly_plan_1_loaded_name = $yearly_plan_1_name.attr("loaded-name");

        var $monthly_plan_1_id = $("#monthly_plan_1_id");
        var $yearly_plan_1_id = $("#yearly_plan_1_id");

        $($monthly_plan_1_name_trigger).on('click', function (e) {
            trigger_monthly_plan_1_name();
        })

        $($yearly_plan_1_name_trigger).on('click', function (e) {
            trigger_yearly_plan_1_name();
        })

        $monthly_plan_1_id.on('change', function (e) {
            trigger_monthly_plan_1_name();
        })

        $yearly_plan_1_id.on('change', function (e) {
            trigger_yearly_plan_1_name();
        })

        function trigger_monthly_plan_1_name() {
            $monthly_plan_1_name.val(monthly_plan_1_loaded_name);

            if ($monthly_plan_1_name_trigger.is(':checked')) {

                var put_text = $.trim($("#monthly_plan_1_id option:selected").text());

                if (put_text != "Choose a plan") {
                    $monthly_plan_1_name.val(put_text);
                }

            }
        }

        function trigger_yearly_plan_1_name() {
            $yearly_plan_1_name.val(yearly_plan_1_loaded_name);

            if ($yearly_plan_1_name_trigger.is(':checked')) {

                var put_text = $.trim($("#yearly_plan_1_id option:selected").text());
                if (put_text != "Choose a plan") {
                    $yearly_plan_1_name.val(put_text);
                }

            }
        }

        //--------------------------------------------------------------------------------------------------------------

        var settings_form = $("#settings-form");

        settings_form.on("submit", function (e) {
            var ret = true;
            var err = 0;
            var both_empty = false;

            if ($monthly_plan_1_id.val() == "" && $yearly_plan_1_id.val() == "") {
                both_empty = true;
            }
            if (($monthly_plan_1_id.val() == $yearly_plan_1_id.val()) && !both_empty) {
                alert("Chosen plans must be different");
                err++;
            }

            ret = err == 0 ? true : false;

            return ret;
        })
    })
</script>




