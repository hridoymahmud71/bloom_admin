<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <style>
        .red-border {
            border: 1px solid crimson;
        }
    </style>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">

                        <!-- flash <starts> -->
                        <?php if ($this->session->flashdata('flash')) { ?>
                            <?php $panel_type = 'panel-success';
                            if ($this->session->flashdata('success')) {
                                $panel_type = 'panel-success';
                            }
                            if ($this->session->flashdata('error')) {
                                $panel_type = 'panel-danger';
                            }
                            ?>
                            <div class="panel <?= $panel_type; ?>">
                                <div class="panel-heading">
                                    <?= $this->session->flashdata('success') ? $this->session->flashdata('success') : '' ?>
                                    <?= $this->session->flashdata('error') ? $this->session->flashdata('error') : '' ?>
                                </div>
                                <div class="panel-body">
                                    <?= $this->session->flashdata('single_landlord_email_success') ? $this->session->flashdata('single_landlord_email_success') : '' ?>
                                    <?= $this->session->flashdata('multi_landlord_email_success') ? $this->session->flashdata('multi_landlord_email_success') : '' ?>
                                    <?= $this->session->flashdata('multi_landlord_email_error') ? $this->session->flashdata('multi_landlord_email_error') : '' ?>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- flash <ends> -->

                        <h3 class="box-title">
                            <button style="display: none;" class="btn btn-success" onclick="add_owner()"><i
                                        class="glyphicon glyphicon-plus"></i> Add Owner
                            </button>
                            <button class="btn btn-default" onclick="reload_table()"><i
                                        class="glyphicon glyphicon-refresh"></i> Reload
                            </button>
                            &nbsp;
                            <button class="btn btn-warning" onclick="multi_email_modal()">
                                <i class="glyphicon glyphicon-envelope"></i>
                                &nbsp;
                                Email all landlords
                            </button>
                        </h3>
                    </div>

                    <div class="box-body">
                        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th style="width:125px;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>


    </section>


    <script type="text/javascript">

        var save_method; //for save method string
        var table;
        document.addEventListener('DOMContentLoaded', function () {

            //extra <starts>
            $('.see_password').hover(function () {
                $('.password').attr('type', 'text');
            }, function () {
                $('.password').attr('type', 'password');
            });
            //extra <ends>


            //datatables
            table = $('#table').DataTable({

                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                //order: [], //Initial no order.

                // Load data for the table's content from an Ajax source
                ajax: {
                    url: "owner/ajax_list",
                    type: "post",
                    complete: function (res) {

                    }
                },


                //Set column definition initialisation properties.
                columnDefs: [
                    {
                        "targets": [-1], //last column
                        "orderable": false, //set not orderable
                    },
                ],

            });

            //datepicker
            $('.datepicker').datepicker({
                autoclose: true,
                format: "yyyy-mm-dd",
                todayHighlight: true,
                orientation: "top auto",
                todayBtn: true,
                todayHighlight: true,
            });

            //set input/textarea/select event when change value, remove class error and remove text help block
            $("input").change(function () {
                $(this).parent().parent().removeClass('has-error');
                $(this).next().empty();
            });
            $("textarea").change(function () {
                $(this).parent().parent().removeClass('has-error');
                $(this).next().empty();
            });
            $("select").change(function () {
                $(this).parent().parent().removeClass('has-error');
                $(this).next().empty();
            });

        });


        function add_owner() {
            save_method = 'add';
            $('#form')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $('[name="email"]').removeAttr('readonly');
            $('#modal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Add Owner'); // Set Title to Bootstrap modal title
        }

        function edit_owner(id) {
            save_method = 'update';
            $('#form')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string

            //Ajax Load data from ajax
            $.ajax({
                url: "owner/ajax_edit/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {

                    $('[name="user_id"]').val(data.user_id);
                    $('[name="user_fname"]').val(data.user_fname);
                    $('[name="user_lname"]').val(data.user_lname);
                    $('[name="phone"]').val(data.phone);
                    $('[name="email"]').val(data.email).attr('readonly', 'readonly');
                    $('[name="user_status"]').val(data.user_status).trigger('change');
                    $('[name="password"]').val();
                    $('[name="confirm_password"]').val();

                    $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                    $('.modal-title').text('Edit Owner'); // Set title to Bootstrap modal title

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    alert('Error get data from ajax');
                }
            });
        }

        function reload_table() {
            table.ajax.reload(null, false); //reload datatable ajax
        }

        function save() {
            $('#btnSave').text('saving...'); //change button text
            $('#btnSave').attr('disabled', true); //set button disable
            var url;

            if (save_method == 'add') {
                url = "owner/ajax_add";
            } else {
                url = "owner/ajax_update";
            }

            // ajax adding data to database
            $.ajax({
                url: url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function (data) {

                    if (data.status) //if success close modal and reload ajax table
                    {
                        $('#modal_form').modal('hide');
                        reload_table();
                    }
                    else {
                        for (var i = 0; i < data.inputerror.length; i++) {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                        }
                    }
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled', false); //set button enable


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error adding / update data');
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled', false); //set button enable

                }
            });
        }

        function delete_owner(id) {
            if (confirm('Are you sure delete this data?')) {
                // ajax delete data to database
                $.ajax({
                    url: "owner/ajax_delete/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function (data) {
                        //if success reload ajax table
                        $('#modal_form').modal('hide');
                        reload_table();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error deleting data');
                    }
                });

            }
        }

    </script>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Owner Form</h3>
                </div>
                <div class="modal-body form">
                    <form action="#" id="form" class="form-horizontal">
                        <input type="hidden" value="" name="user_id"/>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">First Name</label>
                                <div class="col-md-9">
                                    <input name="user_fname" placeholder="First Name" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Last Name</label>
                                <div class="col-md-9">
                                    <input name="user_lname" placeholder="Last Name" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Phone</label>
                                <div class="col-md-9">
                                    <input name="phone" placeholder="Phone" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Email</label>
                                <div class="col-md-9">
                                    <input name="email" placeholder="Email" class="form-control" type="email">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Status</label>
                                <div class="col-md-9">
                                    <select name="user_status" class="form-control">
                                        <option value="">--Select Status--</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    Password
                                </label>
                                <div class="col-md-9">
                                    <input name="password" placeholder="Password" class="form-control password"
                                           type="password">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    Confirm password
                                </label>
                                <div class="col-md-9">
                                    <input name="confirm_password" placeholder="Confirm password"
                                           class="form-control password" type="password">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 text-center">
                                    <span class="see_password">See Passwords &nbsp;<i class="fa fa-eye"></i></span>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

    </div><!-- /.modal -->
    <!-- End Bootstrap modal -->

    <!-- Bootstrap modal -->
    <div class="modal fade" id="single_email_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Email to: &nbsp;
                        <span style="font-style: italic">
                            <span id="single_owner_first_name"></span>&nbsp;
                            <span id="single_owner_last_name"></span>
                        </span>

                    </h3>
                </div>
                <form action="owner/send_email_to_landlord" id="single_email_form" class="form-horizontal" method="post"
                      enctype="multipart/form-data">
                    <div class="modal-body form">
                        <input type="hidden" value="" name="user_id"/>
                        <div class="form-body">
                            <div class="form-group">
                                <input type="hidden" value="" name="id">
                                <label class="control-label col-md-3">Email Address</label>
                                <div class="col-md-9">
                                    <input name="email" id="single_owner_email"
                                           placeholder="Enter Property Owner/Landlords email address"
                                           class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Subject</label>
                                <div class="col-md-9">
                                    <input name="subject" id="single_owner_subject" placeholder="Enter the subject"
                                           class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Subject</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="email_body"
                                              id="single_owner_email_body"></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

    </div><!-- /.modal -->
    <!-- End Bootstrap modal -->

    <!-- Bootstrap modal -->
    <div class="modal fade" id="multi_email_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Email to all landlords &nbsp;
                    </h3>
                </div>
                <form action="owner/send_email_to_landlords" id="multi_email_form" class="form-horizontal" method="post"
                      enctype="multipart/form-data">
                    <div class="modal-body form">
                        <div class="form-group">
                            <label class="control-label col-md-3">Subject</label>
                            <div class="col-md-9">
                                <input name="subject" id="multi_owner_subject" placeholder="Enter the subject"
                                       class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Subject</label>
                            <div class="col-md-9">
                                    <textarea class="form-control" name="email_body"
                                              id="multi_owner_email_body"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->
<!-- End Bootstrap modal -->

</div>

<script>
    function single_email_modal(id) {

        var $email_button = $('#single_email_button_' + id);
        var $single_email_modal = $('#single_email_modal');

        var $single_owner_email = $('#single_owner_email');
        var $single_owner_subject = $('#single_owner_subject');
        var $single_owner_email_body = $('#single_owner_email_body');

        var $single_owner_first_name = $('#single_owner_first_name');
        var $single_owner_last_name = $('#single_owner_last_name');

        var email = $email_button.attr('owner-email');
        var fname = $email_button.attr('owner-fname');
        var lname = $email_button.attr('owner-lname');

        if (email != "") {
            $single_owner_email.val(email);
            $single_owner_email.attr('readonly', 'readonly');
        } else {
            $single_owner_email.removeAttr('readonly');
        }
        if (fname != "") {
            $single_owner_first_name.html(fname);
        }
        if (lname != "") {
            $single_owner_last_name.html(lname);
        }


        $single_owner_email.removeClass('red-border');
        $single_owner_subject.removeClass('red-border');
        $single_owner_email_body.removeClass('red-border');


        $single_email_modal.modal('show');

    }

    document.addEventListener("DOMContentLoaded", function (event) {

        $("#single_email_form").on("submit", function () {

            var ret = true;

            var $single_owner_email = $('#single_owner_email');
            var $single_owner_subject = $('#single_owner_subject');
            var $single_owner_email_body = $('#single_owner_email_body');

            if (!validate_single_email_form($single_owner_email, $single_owner_subject, $single_owner_email_body)) {
                ret = false;
            }

            return ret;

        });


    });


    function validate_single_email_form($single_owner_email, $single_owner_subject, $single_owner_email_body) {

        var err = 0;
        var ret = true;

        if ($single_owner_email.val() == '') {
            err++;
            $single_owner_email.addClass('red-border');
        } else {
            $single_owner_email.removeClass('red-border');
        }

        if ($single_owner_subject.val() == '') {
            err++;
            $single_owner_subject.addClass('red-border');
        } else {
            $single_owner_subject.removeClass('red-border');
        }

        if ($single_owner_email_body.val() == '') {
            err++;
            $single_owner_email_body.addClass('red-border');
        } else {
            $single_owner_email_body.removeClass('red-border');
        }

        if (err > 0) {
            ret = false;
        }

        return ret;

    }
</script>

<script>
    function multi_email_modal(id) {


        var $multi_email_modal = $('#multi_email_modal');

        var $multi_owner_email = $('#multi_owner_email');
        var $multi_owner_subject = $('#multi_owner_subject');
        var $multi_owner_email_body = $('#multi_owner_email_body');

        $multi_owner_email.removeClass('red-border');
        $multi_owner_subject.removeClass('red-border');
        $multi_owner_email_body.removeClass('red-border');


        $multi_email_modal.modal('show');

    }

    document.addEventListener("DOMContentLoaded", function (event) {

        $("#multi_email_form").on("submit", function () {

            var ret = true;

            var $multi_owner_subject = $('#multi_owner_subject');
            var $multi_owner_email_body = $('#multi_owner_email_body');

            if (!validate_multi_email_form($multi_owner_subject, $multi_owner_email_body)) {
                ret = false;
            }

            return ret;

        });


    });


    function validate_multi_email_form($multi_owner_subject, $multi_owner_email_body) {

        var err = 0;
        var ret = true;


        if ($multi_owner_subject.val() == '') {
            err++;
            $multi_owner_subject.addClass('red-border');
        } else {
            $multi_owner_subject.removeClass('red-border');
        }

        if ($multi_owner_email_body.val() == '') {
            err++;
            $multi_owner_email_body.addClass('red-border');
        } else {
            $multi_owner_email_body.removeClass('red-border');
        }

        if (err > 0) {
            ret = false;
        }

        return ret;

    }
</script>






