<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentsettings extends Admin_Controller
{
//https://mbahcoding.com/tutorial/php/codeigniter/codeigniter-ajax-crud-using-bootstrap-modals-and-datatable.html

    public function __construct()
    {
        parent::__construct();
        require_once(APPPATH . 'libraries/stripe-php/init.php');

        $this->load->model('payment_settings_model', 'payment_settings');

        $this->load->helper('url');
        /* Load :: Common */
        //$this->lang->load('admin/users');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_payment_settings'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_payment_settings'), 'admin/menu_payment_settings');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $stripe_settings = $this->payment_settings->getStripeSettings();

            $monthly_plan_1_id = null;
            $yearly_plan_1_id = null;
            $monthly_plan_1_name = null;
            $yearly_plan_1_name = null;
            $test_mode = null;
            $tsk = null;
            $tpk = null;
            $lsk = null;
            $lpk = null;
            $signing_secret = null;

            $sk = $this->payment_settings->get_stripe_secret_key();
            $pk = $this->payment_settings->get_stripe_public_key();

            $plans = array();
            $errors = array();
            try{
                \Stripe\Stripe::setApiKey($sk);
                $plan_obj = \Stripe\Plan::all(["limit" => 500]);
                if ($plan_obj) {
                    $plans = $plan_obj->data;
                }
            }catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught
                $errors[] = $e->getMessage();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $errors[] = $e->getMessage();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
                $errors[] = $e->getMessage();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $errors[] = $e->getMessage();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                $errors[] = $e->getMessage();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $errors[] = $e->getMessage();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $errors[] = $e->getMessage();
            }

            $error_message = "";
            if(!empty($errors)){
                $error_message = implode(" | ",$errors);
            }




            if (!empty($stripe_settings)) {
                $monthly_plan_1_id = isset($stripe_settings['monthly_plan_1_id']) ? $stripe_settings['monthly_plan_1_id'] : null;
                $yearly_plan_1_id = isset($stripe_settings['yearly_plan_1_id']) ? $stripe_settings['yearly_plan_1_id'] : null;
                $monthly_plan_1_name = isset($stripe_settings['monthly_plan_1_name']) ? $stripe_settings['monthly_plan_1_name'] : null;
                $yearly_plan_1_name = isset($stripe_settings['yearly_plan_1_name']) ? $stripe_settings['yearly_plan_1_name'] : null;

                $test_mode = isset($stripe_settings['test_mode']) ? $stripe_settings['test_mode'] : null;
                $tsk = isset($stripe_settings['tsk']) ? $stripe_settings['tsk'] : null;
                $tpk = isset($stripe_settings['tpk']) ? $stripe_settings['tpk'] : null;
                $lsk = isset($stripe_settings['lsk']) ? $stripe_settings['lsk'] : null;
                $lpk = isset($stripe_settings['lpk']) ? $stripe_settings['lpk'] : null;
                $signing_secret = isset($stripe_settings['signing_secret']) ? $stripe_settings['signing_secret'] : null;
            }

            $this->data['plans'] = $plans;
            $this->data['error_message'] = $error_message;

            $this->data['monthly_plan_1_id'] = $monthly_plan_1_id;
            $this->data['yearly_plan_1_id'] = $yearly_plan_1_id;
            $this->data['monthly_plan_1_name'] = $monthly_plan_1_name;
            $this->data['yearly_plan_1_name'] = $yearly_plan_1_name;
            $this->data['test_mode'] = $test_mode;
            $this->data['tsk'] = $tsk;
            $this->data['tpk'] = $tpk;
            $this->data['lsk'] = $lsk;
            $this->data['lpk'] = $lpk;
            $this->data['signing_secret'] = $signing_secret;



            /* Load Template */
            $this->template->admin_render('admin/payment_settings/payment_settings_view', $this->data);
        }
    }

    public function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }


    public function process_settings()
    {
        $p_data = $this->input->post();
        if (!empty($p_data)) {
            foreach ($p_data as $settings_key => $settings_val) {

                if (!empty($settings_val)) {
                    $this->setPaymentSettings($settings_key, trim($settings_val));
                }
                if (empty($settings_val)) {
                    $this->setPaymentSettings($settings_key, null);
                }
            }
        }

        redirect('admin/paymentsettings');
    }

    public function setPaymentSettings($settings_key, $settings_val)
    {
        $this->payment_settings->setSettings($settings_type = "stripe", $settings_key, $settings_val);
    }



}